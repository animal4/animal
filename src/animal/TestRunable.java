/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author tud08
 */
public class TestRunable {
    public static void main(String[] args) {
        Human human = new Human("MuMu");
        Cat cat = new Cat("Ching");
        Car car = new Car("Engine1");
        Plane plane = new Plane("Engine1");
        human.run();
        cat.run();
        car.run();
        plane.run();
        
        Runable[] runables = {human, cat, car, plane};
        for(Runable r : runables){
            r.run();
        }
    }
}
