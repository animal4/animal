/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author tud08
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human thuch = new Human("Thuch");
        thuch.eat();
        thuch.walk();
        thuch.run();
        thuch.speak();
        thuch.sleep();
        System.out.println("Thuch is Animal ? "+ (thuch instanceof Animal));
        System.out.println("Thuch is Land Animal ? "+ (thuch instanceof LandAnimal));
        Cat ru = new Cat("Ru");
        ru.eat();
        ru.walk();
        ru.run();
        ru.speak();
        ru.sleep();
        System.out.println("Ru is Animal ? "+ (ru instanceof Animal));
        System.out.println("Ru is Land Animal ? "+ (ru instanceof LandAnimal));
        Dog dang = new Dog("Dang");
        dang.eat();
        dang.walk();
        dang.run();
        dang.speak();
        dang.sleep();
        System.out.println("Dang is Animal ? "+ (dang instanceof Animal));
        System.out.println("Dang is Land Animal ? "+ (dang instanceof LandAnimal));
        Crocodile ngoy = new Crocodile("Ngoy");
        ngoy.eat();
        ngoy.walk();
        ngoy.crawl();
        ngoy.speak();
        ngoy.sleep();
        System.out.println("Ngoy is Animal ? "+ (ngoy instanceof Animal));
        System.out.println("Ngoy is Land Animal ? "+ (ngoy instanceof Reptile));
        Snake art = new Snake("Art");
        art.eat();
        art.walk();
        art.crawl();
        art.speak();
        art.sleep();
        System.out.println("Art is Animal ? "+ (art instanceof Animal));
        System.out.println("Art is Land Animal ? "+ (art instanceof Reptile));
        Crab to = new Crab("To");
        to.eat();
        to.walk();
        to.swim();
        to.speak();
        to.sleep();
        System.out.println("To is Animal ? "+ (to instanceof Animal));
        System.out.println("To is Land Animal ? "+ (to instanceof AquaticAnimal));
        Fish song = new Fish("Song");
        song.eat();
        song.walk();
        song.swim();
        song.speak();
        song.sleep();
        System.out.println("Song is Animal ? "+ (song instanceof Animal));
        System.out.println("Song is Land Animal ? "+ (song instanceof AquaticAnimal));
        Bat boot = new Bat("Boot");
        boot.eat();
        boot.walk();
        boot.fly();
        boot.speak();
        boot.sleep();
        System.out.println("Boot is Animal ? "+ (boot instanceof Animal));
        System.out.println("Boot is Land Animal ? "+ (boot instanceof Poultry));
        Bird aom = new Bird("Aom");
        aom.eat();
        aom.walk();
        aom.fly();
        aom.speak();
        aom.sleep();
        System.out.println("Aom is Animal ? "+ (aom instanceof Animal));
        System.out.println("Aom is Land Animal ? "+ (aom instanceof Poultry));
    }
}
