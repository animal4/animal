/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author tud08
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("Som");
        Plane plane = new Plane("Engine1");
        bat.fly();
        plane.fly();
        
        Flyable[] flyables = {bat, plane};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
    }
}